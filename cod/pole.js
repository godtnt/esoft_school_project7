class GameField {
    // state - поле игры
    state = [
        [null, null, null],
        [null, null, null],
        [null, null, null]
    ];
    // setMode - метод для понимания чей ход, xodX = true - ход Крестиков, xodX = false - Ноликов
    setMode = function () {
        let xodX = null;

        if ((this.state.flat().filter(val => val != null).length) % 2 === 0) {
            xodX = true;
        } else {
            xodX = false;
        };
        
        return xodX
    };
    // isOverGame - метод для понимания завершилась ли игра
    isOverGame = function () {
        let isOverGame = null;
        if ((winXCondition(this.state.flat()) == true) || (winOCondition(this.state.flat()) == true)) {
            isOverGame = true
        } else {
            isOverGame = false
        }
        return isOverGame
    };
}

// Функции winXCondition и winOCondition функции для проверки победы Крестиков и Ноликов соответственно
function winXCondition (arrey) {
    const firstCell = arrey[0]; const secondCell = arrey[1]; const thirdCell = arrey[2]; const fourthCell = arrey[3]; const fifthCell = arrey[4]; const sixthCell = arrey[5]; const seventhCell = arrey[6]; const eighthCell = arrey[7]; const ninthCell = arrey[8];
    if ((firstCell == 'x' && secondCell == 'x' && thirdCell == 'x') || (fourthCell == 'x' && fifthCell == 'x' && sixthCell == 'x') || (seventhCell == 'x' && eighthCell == 'x' && ninthCell == 'x') || (firstCell == 'x' && fourthCell == 'x' && seventhCell == 'x') || (secondCell == 'x' && fifthCell == 'x' && eighthCell == 'x') || (thirdCell == 'x' && sixthCell == 'x' && ninthCell == 'x') || (firstCell == 'x' && fifthCell == 'x' && ninthCell == 'x') || (thirdCell == 'x' && fifthCell == 'x' && seventhCell == 'x')) {
        winX = true
    } else {
        winX = false
    }
    return winX
}
function winOCondition (arrey) {
    const firstCell = arrey[0]; const secondCell = arrey[1]; const thirdCell = arrey[2]; const fourthCell = arrey[3]; const fifthCell = arrey[4]; const sixthCell = arrey[5]; const seventhCell = arrey[6]; const eighthCell = arrey[7]; const ninthCell = arrey[8];
    if ((firstCell == 'o' && secondCell == 'o' && thirdCell == 'o') || (fourthCell == 'o' && fifthCell == 'o' && sixthCell == 'o') || (seventhCell == 'o' && eighthCell == 'o' && ninthCell == 'o') || (firstCell == 'o' && fourthCell == 'o' && seventhCell == 'o') || (secondCell == 'o' && fifthCell == 'o' && eighthCell == 'o') || (thirdCell == 'o' && sixthCell == 'o' && ninthCell == 'o') || (firstCell == 'o' && fifthCell == 'o' && ninthCell == 'o') || (thirdCell == 'o' && fifthCell == 'o' && seventhCell == 'o')) {
        winO = true
    } else {
        winO = false
    }
    return winO
}
const gameField = new GameField();

const xodInClell1 = function () {
    gameStap()
    if ((gameField.state[0][0] === null) && (gameField.setMode() == true)) {
        gameField.state[0][0] = 'x'
        cell1.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-x.svg"/>`)
    } else if  ((gameField.state[0][0] === null) && (gameField.setMode() !== true)) {
        gameField.state[0][0] = 'o'
        cell1.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-zero.svg"/>`)
    }
    winGame()
}
const xodInClell2 = function () {
    gameStap()
    if ((gameField.state[0][1] === null) && (gameField.setMode() == true)) {
        gameField.state[0][1] = 'x'
        cell2.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-x.svg"/>`)
    } else if  ((gameField.state[0][1] === null) && (gameField.setMode() !== true)) {
        gameField.state[0][1] = 'o'
        cell2.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-zero.svg"/>`)
    }
    winGame()
}
const xodInClell3 = function () {
    gameStap()
    if ((gameField.state[0][2] === null) && (gameField.setMode() == true)) {
        gameField.state[0][2] = 'x'
        cell3.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-x.svg"/>`)
    } else if  ((gameField.state[0][2] === null) && (gameField.setMode() !== true)) {
        gameField.state[0][2] = 'o'
        cell3.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-zero.svg"/>`)
    }
    winGame()
}
const xodInClell4 = function () {
    gameStap()
    if ((gameField.state[1][0] === null) && (gameField.setMode() == true)) {
        gameField.state[1][0] = 'x'
        cell4.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-x.svg"/>`)
    } else if  ((gameField.state[1][0] === null) && (gameField.setMode() !== true)) {
        gameField.state[1][0] = 'o'
        cell4.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-zero.svg"/>`)
    }
    winGame()
}
const xodInClell5 = function () {
    gameStap()
    if ((gameField.state[1][1] === null) && (gameField.setMode() == true)) {
        gameField.state[1][1] = 'x'
        cell5.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-x.svg"/>`)
    } else if  ((gameField.state[1][1] === null) && (gameField.setMode() !== true)) {
        gameField.state[1][1] = 'o'
        cell5.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-zero.svg"/>`)
    }
    winGame()
}
const xodInClell6 = function () {
    gameStap()
    if ((gameField.state[1][2] === null) && (gameField.setMode() == true)) {
        gameField.state[1][2] = 'x'
        cell6.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-x.svg"/>`)
    } else if  ((gameField.state[1][2] === null) && (gameField.setMode() !== true)) {
        gameField.state[1][2] = 'o'
        cell6.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-zero.svg"/>`)
    }
    winGame()
}
const xodInClell7 = function () {
    gameStap()
    if ((gameField.state[2][0] === null) && (gameField.setMode() == true)) {
        gameField.state[2][0] = 'x'
        cell7.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-x.svg"/>`)
    } else if  ((gameField.state[2][0] === null) && (gameField.setMode() !== true)) {
        gameField.state[2][0] = 'o'
        cell7.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-zero.svg"/>`)
    }
    winGame()
}
const xodInClell8 = function () {
    gameStap()
    if ((gameField.state[2][1] === null) && (gameField.setMode() == true)) {
        gameField.state[2][1] = 'x'
        cell8.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-x.svg"/>`)
    } else if  ((gameField.state[2][1] === null) && (gameField.setMode() !== true)) {
        gameField.state[2][1] = 'o'
        cell8.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-zero.svg"/>`)
    }
    winGame()
}
const xodInClell9 = function () {
    gameStap()
    if ((gameField.state[2][2] === null) && (gameField.setMode() == true)) {
        gameField.state[2][2] = 'x'
        cell9.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-x.svg"/>`)
    } else if  ((gameField.state[2][2] === null) && (gameField.setMode() !== true)) {
        gameField.state[2][2] = 'o'
        cell9.insertAdjacentHTML("afterbegin", `<img src="../imgs/xxl-zero.svg"/>`)
    }
    winGame()
}

function winGame () {
    if (winXCondition(gameField.state.flat()) == true) {
        alert(`Крестики победили`)
    } else if (winOCondition(gameField.state.flat()) == true) {
        alert(`Нолики победили`)
    } else if (gameField.state.flat().filter(val => val != null).length === 9) {
        alert(`Ничья`)
    }
}
function gameStap () {
    if (gameField.setMode() == true) {
        step.remove()
        board.insertAdjacentHTML("afterend", `<div id="step">Ходят&nbsp;<img src="../imgs/zero.svg" />&nbsp;Нолики</div>`)
    } else if (gameField.setMode() !== true) {
        step.remove()
        board.insertAdjacentHTML("afterend", `<div id="step">Ходят&nbsp;<img src="../imgs/x.svg" />&nbsp;Крестики</div>`)
    }
}

document.getElementById("cell1").addEventListener("click", xodInClell1)
document.getElementById("cell2").addEventListener("click", xodInClell2)
document.getElementById("cell3").addEventListener("click", xodInClell3)
document.getElementById("cell4").addEventListener("click", xodInClell4)
document.getElementById("cell5").addEventListener("click", xodInClell5)
document.getElementById("cell6").addEventListener("click", xodInClell6)
document.getElementById("cell7").addEventListener("click", xodInClell7)
document.getElementById("cell8").addEventListener("click", xodInClell8)
document.getElementById("cell9").addEventListener("click", xodInClell9)